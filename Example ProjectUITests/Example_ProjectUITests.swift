//
//  Example_ProjectUITests.swift
//  Example ProjectUITests
//
//  Created by Pawel Sulik on 09.01.19.
//  Copyright © 2019 Pawel Sulik. All rights reserved.
//

import XCTest

class Example_ProjectUITests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        let app = XCUIApplication()
        let tablesQuery = app.tables
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["Feature 1"]/*[[".cells.staticTexts[\"Feature 1\"]",".staticTexts[\"Feature 1\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        
        let element = app.otherElements.containing(.navigationBar, identifier:"Feature 1").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        element.swipeDown()
        element.swipeUp()
        app.navigationBars["Feature 1"].buttons["Features List"].tap()
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["Feature 2"]/*[[".cells.staticTexts[\"Feature 2\"]",".staticTexts[\"Feature 2\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        app.navigationBars["Feature 2"].buttons["Features List"].tap()
                
    }

}
