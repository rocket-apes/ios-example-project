//
//  ViewController.swift
//  Example Project
//
//  Created by Pawel Sulik on 09.01.19.
//  Copyright © 2019 Pawel Sulik. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.title = "Features List"
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    //MARK: - Table View Data Source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "featureCell")!
        
        cell.textLabel?.text = "Feature \(indexPath.row + 1)"
        
        return cell
    }
    //MARK: - Table View Delegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "featureVC")
        {
            self.navigationController?.pushViewController(vc, animated: true)
            vc.title = "Feature \(indexPath.item + 1)"
        }
    }
}

